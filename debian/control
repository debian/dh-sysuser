Source: dh-sysuser
Section: admin
Priority: optional
Maintainer: Lorenzo Puliti <plorenzo@disroot.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://salsa.debian.org/debian/dh-sysuser
Vcs-Git: https://salsa.debian.org/debian/dh-sysuser.git
Vcs-Browser: https://salsa.debian.org/debian/dh-sysuser

Package: dh-sysuser
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}, ${perl:Depends}
Provides: dh-sequence-sysuser
Description: alternative debhelper addon to create system users
 dh-sysuser is an alternative to the more popular dh-installsysusers addon.
 dh-sysuser provides a debhelper sequence addon named 'sysuser' and
 a command 'dh_sysuser' which installs sysusers.d conf files and ensure that
 required system users are present after package installation.

Package: sysuser-helper
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}, ${perl:Depends}, adduser, passwd
Description: dh-sysuser implementation detail
 sysuser-helper provides minsysusers, a sysusers.d(5) conf file parser that
 is used as a fallback code by dh-sysuser package. It may also provide code to
 perform additional features on behalf of dh-sysuser package.
 This separation allows packages to take advantage of improvement or fixes
 in 'dh-sysuser' without rebuilding.
 .
 This package is implementation detail of 'dh-sysuser'. No assumption
 about its content can be made.
