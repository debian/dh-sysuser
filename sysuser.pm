#!/usr/bin/perl
use warnings;
use strict;
use Debian::Debhelper::Dh_Lib;

# must run after dh_installsysusers, bad interactions otherwise
insert_after('dh_installsysusers', 'dh_sysuser');
insert_before('dh_install', 'dh_sysuser');

1;
